#include "pch.h"
#include <iostream>

const char N = 27;
char alpha[N] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
char rotor1[N] = "EKMFLGDQVZNTOWYHXUSPAIBRCJ";
char rotor2[N] = "ESOVPZJAYQUIRHXLNFTGKDCMWB";
char rotor3[N] = "VZBRGITYUPSDNHLXAWMJQOFECK";
char reflector[N] = "YRUHQSLDPXNGOKMIEBFZCWVJAT";
char word[] = " ";
int rotor_pos1, rotor_pos2, rotor_pos3;
char temp;
int curr_letter;
char sign;
int R;

int word_counter()
{
	int r = 0;
	while (word[r] != '\0')
	{
		r++;
	}
	return r;
}

char char_finder()
{
	int number = 0;
	std::string salpha = alpha;
	std::string srotor3 = rotor3;
	std::string srotor2 = rotor2;
	std::string srotor1 = rotor1;

		//first turn
		number = salpha.find(word[R]);
		sign = rotor1[number];
		
		number = salpha.find(sign);
		sign = rotor2[number];
		
		number = salpha.find(sign);
		sign = rotor3[number];
		
		number = salpha.find(sign);
		sign = reflector[number];
		//second turn

		number = srotor3.find(sign);
		sign = alpha[number];

		number = srotor2.find(sign);
		sign = alpha[number];
		
		number = srotor1.find(sign);
		sign = alpha[number];


		return sign;
}


int main()
{
	std::cout << "enter word that you want to encrypt/decrypt: ";
	std::cin >> word;
	std::cout << "now enter starting positions of rotors: ";
	std::cin >> rotor_pos1 >> rotor_pos2 >> rotor_pos3;
	char word_crypted[256];
	//preparing rotors
	for (int j = rotor_pos1; j > 0; j--)		//preparing rotor1
	{


		temp = rotor1[N - 2];
		for (int i = N - 3; i >= 0; i--)
		{
			rotor1[i + 1] = rotor1[i];
		}

		rotor1[0] = temp;
	}

	for (int k = rotor_pos2 - 1; k > 0; k--)   //preparing rotor2
	{

		temp = rotor2[N - 2];
		for (int i = N - 3; i >= 0; i--)
		{
			rotor2[i + 1] = rotor2[i];
		}

		rotor2[0] = temp;
	}

	for (int l = rotor_pos3 - 1; l > 0; l--)   //preparing rotor3
	{

		temp = rotor3[N - 2];
		for (int i = N - 3; i >= 0; i--)
		{
			rotor3[i + 1] = rotor3[i];
		}

		rotor3[0] = temp;
	}

	word_counter();
	
	for (R = 0; R < word_counter(); R++)
	{

		char_finder();
		
		word_crypted[R] = sign;
		word_crypted[R + 1] = '\0';
		
		temp = rotor1[N - 2];
		for (int i = N - 3; i >= 0; i--)
		{
			rotor1[i + 1] = rotor1[i];
		}

		rotor1[0] = temp;

	}

	std::cout << word << "\n";
	std::cout << word_crypted;

}


